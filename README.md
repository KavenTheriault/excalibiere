# Excalibière
Excalibière est un jeu de déduction basé sur les Chevaliers de la Table Ronde.

## Histoire
  - Les chevaliers sont très saoul
  - Un évènement se produit et Arthur doit absolument retrouver Excalibur, mais il ne se rappelle pas de quoi elle a l'air.
  - Le hic, chaque chevalier a un souvenir nébuleux d'Excalibur
  - Mordred, grand ennemi du Roi Arthur, tente de les empêcher
  - Les chevaliers sont trop saoul pour ce rendre compte qu'un enemie est parmis eux

## But du jeu
- **Équipe roi Arthur**: Assembler les 3 composantes de l'Excalibur et la faire manier par le roi Arthur
- **Équipe Mordred**: Semer la bisbille parmis les chevaliers pour les empêcher de trouver l'Excalibur avant le nombre maximum de tentative

## Mise en place
  - Chaque joueur incarne un chevalier de la table ronde
  - Un des joueurs est désigné en tant que roi Arthur (Tout le monde doit savoir qui jou Arthur)
  - Un des joueurs est désigné en tant que Mordred (Personne ne doit savoir qui jou Mordred)
  - Le jeu génère l'Excalibur en combinant 3 composantes de manière aléatoire
  - Une caractéristique d'effet est associée aléatoirement à chaque composantes (Voir effets plus bas)
  - La main de chaque joueur est déterminé aléatoirement. Elle est constitué d'une partie des composantes disponibles.
  - Le jeu propose une épée de départ aléatoire au chevaliers

## Tour du joueur
Le chevalier jou sur l'épée du joueur précédent (ou celle de départ si début de partie)
1. (Optionnel) Changer une des composantes par une de sa main
2. Manier l'épée pour savoir le nombre de bonnes composantes (gesture de fencing)

Est ce qu'on permet au joueur de passer son tour ?

## Maniement de l'épée
- Le joueur doit prendre le téléphone et faire des mouvements comme s'il fendait l'air.
- Le téléphone devrait vibrer un certain nombre de fois indiquant le nombre de composant qui sont aussi présent dans Excalibur.
- Lorsqu'un joueur manie une épée, cela déclenche l'effet des composantes de celle-ci.

## Composantes de l'épée
Chacun des composants s'ajoute au nom de l'épée qui se doit d'être humoristique.
- **Pommeaux** (Préfixe, i.e. Afuttée, Tranchante, Légère, …)
- **Gardes** (Suffixe, i.e. Brûlante, de la Mort, Brisée, …)
- **Lames** (Type d'épée, i.e. Sabre, Rapière, …)

## Effets spéciaux
L'effet est l'enssemble des caractéristiques de chacune des 3 composantes de l'épée

#### Type d'effet
  - Soif (50%): Boire _X_ gorgées
  - Malédiction (10%): Double la prochaine `Soif` pendant _X_ tour ou annule `Benediction`
  - Bénédiction (10%): Évite la prochaine `Soif` pendant _X_ tour ou annule `Malédiction`
  - Sommeil (10%): Passe _X_ tour
  - Fanatique (10%): Jou _X_ tour de suite
  - Amour (10%): Subisse la `Soif` de leur amoureux pendant _X_ tour

#### Quantité
- 1 (30%)
- 2 (30%)
- 3 (20%)
- 4 (10%)
- 5 (10%)

#### Cible
- Au choix (30%): Le joueur qui manie l'épée décide le chevalier affecté
- Joueur (30%): Le joueur qui manie l'épée est affecté
- Compagnons (20%): Le joueur à gauche et le joueur à droite du joueur qui manie l'épée sont affectés
- Arthur (10%): Arthur est affecté
- Tout le monde sauf Arthur (10%): Tout les joeurs sauf Arthur sont affectés

## Règles supplémentaires
- Tous les effets qui dure _X_ tour ne sont pas accumulables. Pas exemple, si un joueur est présentement affecté par `Malédiction` pour encore deux tours, et qu'il reçoit à nouveau `Malédiction`. Cela n'aura aucun effet sur son nombre de tour restant.
